//import Components from "views/Components/Components.jsx";
import LandingPage from "views/LandingPage/LandingPage.jsx";
import LoginPage from "views/LoginPage/LoginPage.jsx";
import PasswordPage from "views/PasswordPage/PasswordPage.jsx";
//import Test from "views/Test/Test.jsx";
import HomePage from "views/HomePage/HomePage.jsx";
import KategoriPage from "views/KategoriPage/KategoriPage.jsx";
import DaftarPage from "views/DaftarPage/DaftarPage.jsx";
import FilterPage from "views/FilterPage/FilterPage.jsx";
import WarungMenu from "views/WarungPage/Menu/WarungMenu.jsx";
import WarungTelepon from "views/WarungPage/Telepon/WarungTelepon.jsx";
import WarungLokasi from "views/WarungPage/Lokasi/WarungLokasi.jsx";
import WarungReview from "views/WarungPage/Review/WarungReview.jsx";
import ProfilePage from "views/ProfilePage/ProfilePage.jsx";
import WarungkuPage from "views/WarungkuPage/WarungkuPage.jsx";
import WarungAddPage from "views/WarungAdd/WarungAddPage.jsx";
//import LoadMore from "views/Test/loadmore.jsx";
import Logout from "library/Logout.jsx";

var indexRoutes = [
    { path: "/landing-page", name: "LandingPage", component: LandingPage },
    { path: "/login-page", name: "LoginPage", component: LoginPage },
    { path: "/kategori-warung", name: "KateguoriPage", component: KategoriPage },
    { path: "/login", name: "KategoriPage", component: LoginPage },
    { path: "/password", name: "PasswordPage", component: PasswordPage },
    { path: "/daftar", name: "DaftarPage", component: DaftarPage },
    { path: "/filter/:keywords/:price/:time/:day/:filterStatus/:kategori", name: "FilterPage", component: FilterPage, exact: true},
    { path: "/filter/:keywords", name: "FilterPage", component: FilterPage, exact: true},
    { path: '/warung/tambah', name: "WarungAddPage", component: WarungAddPage, exact: true },
    { path: "/warung/:permalink/telepon", name: "WarungTelepon", component: WarungTelepon},
    { path: "/warung/:permalink/lokasi", name: "WarungLokasi", component: WarungLokasi},
    { path: "/warung/:permalink/review", name: "WarungReview", component: WarungReview},
    { path: "/warung/:permalink", name: "WarungMenu", component: WarungMenu},
    //{ path: '/loadmore', name: "LoadMore", component: LoadMore },
    { path: '/warungku', name: "WarungkuPage", component: WarungkuPage },
    { path: '/profile', name: "ProfilePage", component: ProfilePage },
    { path: '/logout', name: "Logout", component: Logout },
    { path: "/", name: "Components", component: HomePage, exact: true },
];

export default indexRoutes;
