const Config = {
    'API_URL': 'http://test.capitalsyariah.com/warungs/api',
    'openTime': '07:00',
    'closeTime': '17:00',
    'priceMin': 0,
    'priceMax': 117000,
    'warungPerPage': 2,
    'offset': 0,
    'keyMap': 'AIzaSyBLgo7ebgMwaEJgPj_LNisAcscNDb9UP-M',
    'facebookId': '2188254351189478',
    'googleId': '988711566757-dvh6fqqa6qn3r1mj5ukadov4plce5puj.apps.googleusercontent.com'
};

export default Config;