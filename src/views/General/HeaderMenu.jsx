import React from "react";

// material-ui components
import withStyles from "material-ui/styles/withStyles";
import List from "material-ui/List";
import ListItem from "material-ui/List/ListItem";
import { Link } from "react-router-dom";
import Button from "components/CustomButtons/Button.jsx";
// @material-ui/icons
import Home from "@material-ui/icons/Home";
import CardTravel from "@material-ui/icons/CardTravel";
// core components
import { AccountCircle } from "@material-ui/icons";
import Header from "./Header.jsx";
import {reactLocalStorage} from 'reactjs-localstorage';

import navbarsStyle from "assets/jss/material-kit-react/views/componentsSections/navbarsStyle.jsx";
import CustomDropdown from 'components/CustomDropdown/CustomDropdown.jsx';

import AddIcon from '@material-ui/icons/Add';
import ButtonUi from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';


class SectionNavbars extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            user: reactLocalStorage.getObject('user'),
            statusLogin: reactLocalStorage.get('statusLogin'),
            headerColor: props.headerColor,
        };
    }

    componentWillMount() {

    }

    render() {
        const { classes } = this.props;
        let user = this.state.user;
        let statusLogin = this.state.statusLogin;
        let headerColor = this.state.headerColor === undefined ? "rose": this.state.headerColor;
        let menu, btnTambahWarung;

        if(statusLogin === 'yes') {
            menu =      <ListItem className={classes.listItem}>
                            <CustomDropdown
                                noLiPadding
                                buttonText={user.name}
                                buttonProps={{
                                    className: classes.navLink,
                                    color: "warning"
                                }}
                                buttonIcon={AccountCircle}
                                dropdownList={[
                                    <Link to="/profile" className={classes.dropdownLink}>
                                        Edit Profil
                                    </Link>,
                                    <Link to="/logout" className={classes.dropdownLink}>
                                        Logout
                                    </Link>,
                                ]}
                            />
                      </ListItem>

            btnTambahWarung = <Tooltip title="Tambah Warung">
                                <Link to="/warung/tambah">
                                    <ButtonUi variant="fab" color="secondary" style={{ position: "fixed",  bottom: "40px", right: "40px", zIndex: "99999999" }}>
                                        <AddIcon />
                                    </ButtonUi>
                                </Link>
                            </Tooltip>
        } else {
            menu = <span>
                    <ListItem className={classes.listItem}>
                        <Link to="/login">
                            <Button
                                className={classes.navLink}
                                color="warning"
                                round
                            >
                                <AccountCircle className={classes.icons} /> Daftar/Login
                            </Button>
                        </Link>
                    </ListItem>
            </span>
        }


        return (
            <div className="header-wrapper">
                <Header
                    brand="WARUNGS"
                    color={headerColor}
                    fixed
                    rightLinks={
                        <List className={classes.list+' fo-header'}>
                            <ListItem className={classes.listItem}>
                                <Link to="/">
                                    <Button
                                        className={classes.navLink}
                                        color="transparent"
                                    >
                                        <Home className={classes.icons} /> Home
                                    </Button>
                                </Link>
                            </ListItem>
                            <ListItem className={classes.listItem}>
                                <Link to="/kategori-warung">
                                    <Button
                                        className={classes.navLink}
                                        color="transparent"
                                    >
                                        <CardTravel className={classes.icons} /> Kategori
                                    </Button>
                                </Link>
                            </ListItem>

                            {menu}
                        </List>
                    }
                />

                {btnTambahWarung}
            </div>
        );
    }
}

export default withStyles(navbarsStyle)(SectionNavbars);
