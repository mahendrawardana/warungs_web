import React from "react";

// Bootstrap
import { Container, Row, Col } from 'reactstrap';

import image from "assets/img/footer.jpg";
import './Footer.css';

class Footer extends React.Component {
    render() {

        let footerProps = {
            backgroundImage: "url(" + image + ")",
            height: "100px",
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center center"
        };

        return (
            <footer style={{...footerProps}} className="footer">
                <Container>
                    <Row>
                        <Col xs="12" md="12">
                            <h3 align="center">Warungs</h3>
                            <p align="center">Copyright 2018 HOOKI GROUP</p>
                        </Col>
                    </Row>
                </Container>
            </footer>
        )
    }
}

export default Footer;