import React from "react";

import { Container, Row, Col } from "reactstrap";
import "./Header.css";

class WarungFormTitle extends React.Component {

    render() {
        return(
            <Container>
                <Row>
                    <Col xs={12}>
                        <br/><br/><br/>
                        <h1 align="center" style={{color:"white"}}>
                            Cari Warungs untuk Makanmu Saat Ini ?
                        </h1>
                        <p align="center" style={{color:"white"}}>Dapatkan Info Warung Makan sesuai Keinginan dan Seleramu Saat Ini</p>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default WarungFormTitle;