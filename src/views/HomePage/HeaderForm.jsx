import React from "react";
import moment from 'moment';
import Config from '../../config';
import String from '../../string';
import { Redirect } from 'react-router-dom'

import 'antd/dist/antd.css';
import "./Header.css";
import navbarsStyle from "assets/jss/material-kit-react/views/componentsSections/navbarsStyle.jsx";

// material-ui components
import withStyles from "material-ui/styles/withStyles";
// core components
import NumberFormat from 'react-number-format';

// Bootstrap
import { InputGroup, InputGroupAddon, Button as ButtonStrap, Input as InputStrap } from 'reactstrap';
import { Container, Row, Col } from 'reactstrap';
//import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Form, FormGroup, Label } from 'reactstrap';
import * as Fa from 'react-icons/lib/fa';

// Antd
import { Slider, Modal, Checkbox, TimePicker, Tag, Button, Tooltip } from 'antd';
const CheckboxGroup = Checkbox.Group;



// Material



class SectionNavbars extends React.Component {
    constructor(props) {
        super(props);

        var keywords = props.keywords === undefined ? '': props.keywords;
        var priceMin = props.priceMin === undefined ? parseInt(Config.priceMin, 10): parseInt(props.priceMin, 10);
        var priceMax = props.priceMax === undefined ? parseInt(Config.priceMax, 10): parseInt(props.priceMax, 10);
        var openTime = props.openTime === undefined ? Config.openTime: props.openTime;
        var closeTime = props.closeTime === undefined ? Config.closeTime: props.closeTime;
        let daySelect = [];
        let kategoriMenuSelect = [];

        if(props.daySelect === undefined) {
            daySelect[0] = String.dayNow;
            kategoriMenuSelect = String.kategoriMenuSelect;
        } else {
            daySelect = props.daySelect;
            kategoriMenuSelect = props.kategoriMenuSelect;
        }

        let filterDataStatus = false;
        if(props.filterStatus !== undefined) {
            if(props.filterStatus === 1) {
                filterDataStatus = true;
            }
        }

        this.state = {
            modal: false,
            priceMin: 0,
            priceMinDefault: priceMin,
            priceMax: 200000,
            priceMaxDefault: priceMax,
            operationDay: [],
            closeTime: closeTime,
            openTime: openTime,
            daySelect: daySelect,
            kategoriMenuSelect: kategoriMenuSelect,
            filterDataStatus: filterDataStatus,
            filterDataValue: keywords,
            tooltipVisible: false,
            redirectStatus: false,
            redirectPath: ''
        };

        this.toggle = this.toggle.bind(this);
    }

    componentDidMount() {
        //console.log(this.state.daySelect);



    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }
    changePrice = (value) => {
        console.log(value[0]);
        this.setState({
            priceMinDefault: value[0],
            priceMaxDefault: value[1]
        });
    }

    setDaySelect = (event) => {
        let day = this.state.daySelect;
        //let key = 0;
        day = event.map((data) => {
            return data;
        });
        this.setState({
            daySelect: day
        });
    }

    setKategoriMenuSelect = (event) => {
        let kategoriMenu = this.state.kategoriMenuSelect;
        //let key = 0;
        kategoriMenu = event.map((data) => {
            return data;
        });
        this.setState({
            kategoriMenuSelect: kategoriMenu
        });

    }

    setFilterDataAktif = () => {
        this.setState({
            filterDataStatus: true,
            modal: false
        });
    }

    setFilterDataPasif = () => {
        this.setState({
            filterDataStatus: false,
            modal: false
        });
    }

    changeOpenTime = (time, timeString) => {
        this.setState({
            openTime: timeString
        });
    }

    changeCloseTime = (time, timeString) => {
        this.setState({
            closeTime: timeString
        });
    }

    setFilterDataValue = (evt) => {
        //alert(evt.target.value);
        this.setState({
            filterDataValue: evt.target.value
        });
    }

    setFilterTooltipHide = () => {
        this.setState({
            tooltipVisible: false
        });
    }

    filterRun = () => {
        let filterDataValue = this.state.filterDataValue;
        let filterDataStatus = this.state.filterDataStatus;
        if(filterDataValue === '') {
            this.setState({
                tooltipVisible: true
            });
        } else {
            let filterDataValue = this.state.filterDataValue;
            let path = '/filter/'+filterDataValue;
            let priceMinDefault = this.state.priceMinDefault;
            let priceMaxDefault = this.state.priceMaxDefault;
            let openTime = this.state.openTime;
            let closeTime = this.state.closeTime;
            let daySelect = this.state.daySelect;
            let kategoriMenuSelect = this.state.kategoriMenuSelect;


            if(filterDataStatus) {
                path += '/'+priceMinDefault+'-'+priceMaxDefault+'/'+openTime+'-'+closeTime+'/'+daySelect+'/1/'+kategoriMenuSelect;
            }
            this.setState({
                redirectStatus: true,
                redirectPath: path
            });
        }
    }

    renderRedirect = () => {
        if(this.state.redirectStatus) {
            return <Redirect to={this.state.redirectPath}/>
        }
    }

    render() {
        //const { classes } = this.props;
        let currency = 'Rp. ';
        let priceMin = this.state.priceMinDefault === 0 ? Config.priceMin : this.state.priceMinDefault;
        let priceMax = this.state.priceMaxDefault === 0 ? Config.priceMax : this.state.priceMaxDefault;
        let daySelect = this.state.daySelect;
        let kategoriMenuSelect = this.state.kategoriMenuSelect;

        let sliderPriceValue = [priceMin, priceMax];
        let filterDataStatus = this.state.filterDataStatus;
/*        const modalProps = {
            isOpen: this.state.modal,
            toggle: this.toggle,
            className: this.props.className,
            fade: false
        };*/

/*        function formatter(value) {
            return (
                <NumberFormat value={value} displayType={'text'} thousandSeparator={true} prefix={currency} renderText={value => <span>{value}</span>} />
            )
        }*/

        function filterDataLabel() {
            if(filterDataStatus) {
                return <Tag color="red">Aktif</Tag>;
            } else {
                return '';
            }
        }

        return (
            <div>
                { this.renderRedirect() }
                <Container className="fo-header">
                    <Row>
                        <Col xs="12">
                            <Tooltip
                                placement="bottom"
                                title="Ketik nama makanan atau minuman yang ingin kamu cari disini."
                                visible={this.state.tooltipVisible}>
                                <InputGroup>
                                    <InputStrap
                                        placeholder="Ketik makan atau minuman keinginanmu disini ..."
                                        className="input-filter"
                                        defaultValue={this.state.filterDataValue}
                                        onKeyUp={this.setFilterDataValue}
                                        onKeyPress={
                                            (event) => {
                                                if(event.key === 'Enter') {
                                                    this.filterRun();
                                                }
                                            }
                                        }
                                        onFocus={ this.setFilterTooltipHide } />
                                    <InputGroupAddon addonType="append">
                                        <ButtonStrap color="default" onClick={this.toggle}><Fa.FaFilter /> Filter Data {filterDataLabel()}</ButtonStrap>
                                        <ButtonStrap color="primary" onClick={this.filterRun}><Fa.FaSearch /> Search</ButtonStrap>
                                    </InputGroupAddon>
                                </InputGroup>
                            </Tooltip>
                        </Col>
                    </Row>
                </Container>
                <Modal
                    title="Filter Data"
                    visible={this.state.modal}
                    onOk={this.setFilterDataAktif}
                    onCancel={this.setFilterDataPasif}
                    width={600}
                    footer={[
                        <Button key="batal" onClick={this.setFilterDataPasif}>Batal</Button>,
                        <Button key="filterdata" type="primary" onClick={this.setFilterDataAktif}>
                            Filter Data
                        </Button>,
                    ]}
                >
                    <Form>
                        <FormGroup>
                            <Label for="examplePassword">Kategori Makanan</Label>
                            <br />
                            <center>
                                <CheckboxGroup options={String.kategoriMenu} defaultValue={kategoriMenuSelect} onChange={this.setKategoriMenuSelect} />
                            </center>
                        </FormGroup>
                        <FormGroup>
                            <Label for="exampleEmail">Rentang Harga</Label>
                            <Row>
                                <Col xs={5} className="text-center">
                                    <NumberFormat value={priceMin} displayType={'text'} thousandSeparator={true} prefix={currency} renderText={value => <span>{value}</span>} />
                                </Col>
                                <Col xs={2} className="text-center">
                                    Sampai
                                </Col>
                                <Col xs={5} className="text-center">
                                    <NumberFormat value={priceMax} displayType={'text'} thousandSeparator={true} prefix={currency} renderText={value => <span>{value}</span>} />
                                </Col>
                            </Row>
                            <Slider range
                                    defaultValue={sliderPriceValue}
                                    min={this.state.priceMin}
                                    max={this.state.priceMax}
                                    onChange={this.changePrice}
                                    tipFormatter={null} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="examplePassword">Hari dan Jam Buka</Label>
                            <br />
                            <center>
                                <CheckboxGroup options={String.dayList} defaultValue={daySelect} onChange={this.setDaySelect} />
                            </center>
                            <br />
                            <center>
                                <TimePicker defaultValue={moment(this.state.openTime, 'HH:mm:ss')} onChange={this.changeOpenTime} /> &nbsp;&nbsp;sampai&nbsp;&nbsp;&nbsp;
                                <TimePicker defaultValue={moment(this.state.closeTime, 'HH:mm:ss')} onChange={this.changeCloseTime} />
                            </center>
                        </FormGroup>
                    </Form>
                </Modal>
            </div>
        );
    }
}

export default withStyles(navbarsStyle)(SectionNavbars);
