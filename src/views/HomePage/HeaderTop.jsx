import React from "react";
import withStyles from "material-ui/styles/withStyles";
import HeaderMenu from "../General/HeaderMenu";
import HeaderForm from "./HeaderForm";
import HeaderFormTitle from "./HeaderFormTitle";

import navbarsStyle from "assets/jss/material-kit-react/views/componentsSections/navbarsStyle.jsx";

import image from "assets/img/header.jpg";
import "./Header.css";

class SectionNavbars extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.section}>
        <div id="navbar" className={classes.navbar}>
          <div>
            <div style={{backgroundImage: "url(" + image + ")", height: "450px", backgroundPosition: "0px 70px", paddingTop: "90px"}}>
              <HeaderMenu/>
              <HeaderFormTitle/>
              <HeaderForm/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(navbarsStyle)(SectionNavbars);
