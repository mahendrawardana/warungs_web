import React from "react";
import withStyles from "material-ui/styles/withStyles";
import HeaderTop from "./HeaderTop.jsx";
import HomeContent from "./HomeContent.jsx";
import Footer from "../General/Footer.jsx";

import componentsStyle from "assets/jss/material-kit-react/views/components.jsx";

import './HomePage.css';

class Components extends React.Component {
    render() {
        //const { classes, ...rest } = this.props;
        return (
            <div>
                <HeaderTop />
                <HomeContent/>
                <Footer/>
            </div>
        );
    }
}

export default withStyles(componentsStyle)(Components);
