import React from 'react';
import Config from '../../config';
import axios from 'axios';

import {Link} from 'react-router-dom';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import FavoriteIcon from '@material-ui/icons/Favorite';
import RemoveRedEye from '@material-ui/icons/RemoveRedEye';
import MatComment from '@material-ui/icons/Comment';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Badge from '@material-ui/core/Badge';

// Bootstrap
import { Container, Row, Col } from 'reactstrap';


import InfiniteScroll from 'react-infinite-scroller';
import qwest from 'qwest';

axios.defaults.baseURL = Config.API_URL;
//axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

const styles = theme => ({
    card: {
        maxWidth: 400,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    actions: {
        display: 'flex',
    },
    expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
        marginLeft: 'auto',
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
});

class RecipeReviewCard extends React.Component {
    state = { expanded: false };

    constructor(props) {
        super(props);

        this.state = {
            warungData: [],
            offset: 0,
            hasMoreItems: true
        }
    }

    handleExpandClick = () => {
        this.setState({ expanded: !this.state.expanded });
    };

    loadItems(page) {
        var self = this;
        var offset = this.state.offset;
        var url = Config.API_URL + '/warung/pagination';

        qwest.post(url, {
            limit: Config.warungPerPage,
            offset: offset
        }, {
            cache: true
        })
            .then(function(xhr, resp) {
                //console.log(resp);
                if(resp) {
                    var warungData = self.state.warungData;
                    resp.data.map((warung) => {
                        warungData.push(warung);
                    });

                    self.setState({
                        warungData: warungData,
                        hasMoreItems: resp.hasMoreData,
                        offset: resp.offset
                    });
                }
            });
    }


    firstLetter = (str) => {
        if(str == '') {
            return 'W';
        } else {
            var matches = str.match(/\b(\w)/g);
            var acronym = matches.join('');
            return acronym;
        }
    }

    render() {
        const loader = <div className="loader">Loading ...</div>;


        const { classes } = this.props;

        let i = 0;
        let warungItem = this.state.warungData.map((data) => {

            let views = data.views ? data.views : 0;
            let love = data.love ? data.love : 0;
            let reviews = data.reviews ? data.reviews : 0;
            let path = `/warung/${data.permalink}`;
            i++;
            return (
                <Col xs="12" sm="4" md="4" key={i}>
                    <br/><br/>
                    <Link to={path}>
                        <Card className={classes.card} key={i}>
                            <CardHeader
                                avatar={
                                    <Avatar aria-label="Recipe" className={classes.avatar}>
                                        {this.firstLetter(data.name)}
                                    </Avatar>
                                }
                                title={data.name}
                                subheader=""
                            />
                            <CardMedia
                                className={classes.media}
                                image={data.thumbnail}
                                title="Contemplative Reptile"
                            />
                            <CardContent>
                                <Typography component="p">
                                    <p align="justify">{data.description.substring(0, 200)} ...</p>
                                </Typography>
                            </CardContent>
                            <CardActions className={classes.actions} disableActionSpacing>
                                <IconButton aria-label="Add to favorites">
                                    <Badge className={classes.margin} badgeContent={love} color="secondary">
                                        <FavoriteIcon />
                                    </Badge>
                                </IconButton>
                                <IconButton aria-label="Add to favorites">
                                    <Badge className={classes.margin} badgeContent={views} color="primary">
                                        <RemoveRedEye />
                                    </Badge>
                                </IconButton>
                                <IconButton aria-label="Add to favorites">
                                    <Badge className={classes.margin} badgeContent={reviews} color="default">
                                        <MatComment/>
                                    </Badge>
                                </IconButton>
                            </CardActions>
                        </Card>
                    </Link>
                </Col>
            )
        });

        return (
            <InfiniteScroll
                pageStart={0}
                loadMore={this.loadItems.bind(this)}
                hasMore={this.state.hasMoreItems}
                loader={loader}>

                <Container className="fo-content">
                    <Row>
                        <Col xs="12">
                            <h2 align="center">Warung Rekomendasi</h2>
                        </Col>
                        {warungItem}
                    </Row>
                </Container>
            </InfiniteScroll>
        );
    }
}

RecipeReviewCard.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(RecipeReviewCard);