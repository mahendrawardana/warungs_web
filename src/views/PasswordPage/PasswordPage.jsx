import React from "react";
// material-ui components
import withStyles from "material-ui/styles/withStyles";
import axios from 'axios';
// core components
import Header from "components/Header/Header.jsx";
import HeaderLinks from "../DaftarPage/DaftarHeaderLinks";
import Footer from "../DaftarPage/DaftarFooter";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import loginPageStyle from "assets/jss/material-kit-react/views/loginPage.jsx";

import {reactLocalStorage} from 'reactjs-localstorage';

import { message, Input, Form } from 'antd';

import image from "assets/img/background-register.jpg";

const FormItem = Form.Item;


class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    let user  = reactLocalStorage.getObject('user');

    this.state = {
      cardAnimaton: "cardHidden",
        form: [],
        token: user.token,
        email: user.email
    };
  }
  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }

    sendLogin = () => {
        const self = this;
        //let user  = reactLocalStorage.getObject('user');

        var headers = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': this.state.token
            }
        }

        message.loading('Sedang Mengirim Data ...', 1);
        axios.post('/user/password', {
            password: this.state.password
        }, headers)
            .then(function (response) {
                // console.log(response.data);
                let resp = response.data;
                if(!resp.success) {
                    const form_self = resp.data;
                    self.setState({
                        form: form_self
                    });
                } else {
                    reactLocalStorage.set('statusLogin', 'yes');
                    reactLocalStorage.setObject('user', resp.data);
                    message.success(resp.message, 1, () => {
                       window.location.href = '/';
                    });
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    setStateIni = (event, field) => {
        var stateObject = function() {
            let returnObj = {};
            returnObj[field] = event.target.value;
            return returnObj;
        }.bind(event)();
        this.setState(stateObject);
    }

  render() {
    const { classes, ...rest } = this.props;
    const { form } = this.state;


      const emailField = form['email'] !== undefined ? 'error' : '';
      const emailLabel = emailField == "error" ? form['email'] : '';
      const passwordField = form['password'] !== undefined ? 'error' : '';
      const passwordLabel = passwordField == "error" ? form['password'] : '';

    return (
      <div>
        <Header
          absolute
          color="transparent"
          brand="WARUNGS"
          rightLinks={<HeaderLinks />}
          {...rest}
        />
        <div
          className={classes.pageHeader}
          style={{
            backgroundImage: "url(" + image + ")",
            backgroundSize: "cover",
            backgroundPosition: "top center"
          }}
        >
          <div className={classes.container}>
            <GridContainer justify="center">
              <GridItem xs={12} sm={12} md={4}>
                <Card className={classes[this.state.cardAnimaton]}>
                  <form className={classes.form}>
                    <CardHeader color="warning" className={classes.cardHeader}>
                      <h4>Membuat Password</h4>
                    </CardHeader>
                    <CardBody>
                        Email : {this.state.email}
                        <FormItem
                            label="Password"
                            validateStatus={passwordField}
                            help={passwordLabel}
                        >
                            <Input type="password" onKeyUp={(e) => this.setStateIni(e, 'password')} />
                        </FormItem>
                    </CardBody>
                    <CardFooter className={classes.cardFooter}>
                      <Button color="warning" size="lg" onClick={this.sendLogin}>
                        Simpan Password
                      </Button>
                    </CardFooter>
                  </form>
                </Card>
              </GridItem>
            </GridContainer>
          </div>
          <Footer whiteFont />
        </div>
      </div>
    );
  }
}

export default withStyles(loginPageStyle)(LoginPage);
