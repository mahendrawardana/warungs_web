import React from "react";
// material-ui components
import withStyles from "material-ui/styles/withStyles";
import Config from '../../config';
import axios from 'axios';
//import {setStateIni} from 'library/Core';
import { Redirect } from 'react-router-dom';
import FacebookLogin from 'react-facebook-login';
import { GoogleLogin } from 'react-google-login';
import { Link } from "react-router-dom";
import FontAwesome from 'react-fontawesome'

// core components
import Header from "components/Header/Header.jsx";
import HeaderLinks from "../DaftarPage/DaftarHeaderLinks";
import Footer from "../DaftarPage/DaftarFooter";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import loginPageStyle from "assets/jss/material-kit-react/views/loginPage.jsx";

import {reactLocalStorage} from 'reactjs-localstorage';

import { message, Input, Form } from 'antd';

import image from "assets/img/background-register.jpg";

const FormItem = Form.Item;


class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    // we use this to make the card to appear after the page has been rendered

      this.responseFacebook = this.responseFacebook.bind(this);
      this.componentClicked = this.componentClicked.bind(this);
      this.responseGoogle = this.responseGoogle.bind(this);

    this.state = {
      cardAnimaton: "cardHidden",
        form: [],
        redirectStatus: false,
        redirectPath: '/'
    };
  }
  componentDidMount() {
    // we add a hidden class to the card and after 700 ms we delete it and the transition appears
    setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }

    sendLogin = () => {
        const self = this;
        message.loading('Sedang Mengirim Data ...', 1);
        axios.post('/user/login', {
            email: this.state.email,
            password: this.state.password
        })
            .then(function (response) {
                // console.log(response.data);
                let resp = response.data;
                if(!resp.success) {
                    const form_self = resp.data;
                    self.setState({
                        form: form_self
                    });
                } else {
                    //console.log(resp.data);
                    reactLocalStorage.set('statusLogin', 'yes');
                    reactLocalStorage.setObject('user', resp.data);
                    reactLocalStorage.set('token', resp.data.token);
                    message.success(resp.message, 3, () => {
                        self.setState({
                            redirectStatus: true,
                            redirectPath: '/'
                        });
                    });
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    setStateIni = (event, field) => {
        var stateObject = function() {
            let returnObj = {};
            returnObj[field] = event.target.value;
            return returnObj;
        }.bind(event)();

        this.setState(stateObject);
    }

    renderRedirect = () => {
        if(this.state.redirectStatus) {
            return <Redirect to={this.state.redirectPath}/>
        }
    }


    componentClicked = () => {
        console.log('Clicked');
    }

    responseFacebook(response){
      console.log(response);
        axios.post('/user/auth_social', {
            name: response.name,
            email: response.email,
            id: response.id,
            token: response.accessToken,
            thumbnail: response.picture.data.url,
            type: 'facebook'
        })
            .then(function (res) {
                let resData = res.data;
                if(resData.success){
                  reactLocalStorage.setObject('user', resData.data.user);
                  reactLocalStorage.set('statusLogin', 'yes');
                  reactLocalStorage.set('token', resData.data.user.token);
                  message.success(resData.message, 1).then(() => {
                      if(resData.data.userInsertPassword) {
                          window.location.href = '/password';
                      } else {
                          window.location.href = '/';
                      }
                  });
                } else {
                    message.error(resData.message, 1);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    responseGoogle = (response) => {
        if(response){
            let profile = response.profileObj;
            let tokenId = response.tokenId;
            console.log(profile, tokenId);

            axios.post('/user/auth_social', {
                name: profile.name,
                email: profile.email,
                id: profile.googleId,
                token: tokenId,
                thumbnail: profile.imageUrl,
                type: 'google'
            })
            .then(function (res) {
                let resData = res.data;
                if(resData.success){
                    reactLocalStorage.setObject('user', resData.data.user);
                    reactLocalStorage.set('statusLogin', 'yes');
                    reactLocalStorage.set('token', resData.data.user.token);
                    message.success(resData.message, 1).then(() => {
                        if(resData.data.userInsertPassword) {
                            window.location.href = '/password';
                        } else {
                            window.location.href = '/';
                        }
                    });
                } else {
                    message.error(resData.message, 1);
                }
            })
            .catch(function (error) {
                console.log(error);
            });

        }
    }

  render() {
    const { classes, ...rest } = this.props;
    const { form } = this.state;


      const emailField = form['email'] !== undefined ? 'error' : '';
      const emailLabel = emailField === "error" ? form['email'] : '';
      const passwordField = form['password'] !== undefined ? 'error' : '';
      const passwordLabel = passwordField === "error" ? form['password'] : '';



    return (
      <div>
          { this.renderRedirect() }
        <Header
          absolute
          color="transparent"
          brand="WARUNGS"
          rightLinks={<HeaderLinks />}
          {...rest}
        />
        <div
          className={classes.pageHeader}
          style={{
            backgroundImage: "url(" + image + ")",
            backgroundSize: "cover",
            backgroundPosition: "top center"
          }}
        >
          <div className={classes.container}>
            <GridContainer justify="center">
              <GridItem xs={12} sm={12} md={4}>
                <Card className={classes[this.state.cardAnimaton]}>
                  <form className={classes.form}>
                    <CardHeader color="warning" className={classes.cardHeader}>
                      <h4>Login</h4>
                    </CardHeader>
                    <CardBody>
                      <center>
                          <FacebookLogin
                              appId={Config.facebookId}
                              autoLoad={false}
                              fields="name,email,picture,id"
                              callback={this.responseFacebook}
                              cssClass="btn-facebook"
                              icon="fa-facebook"
                              textButton=" Login dengan Facebook"
                          />
                          <br />
                          <br />
                          <GoogleLogin
                              clientId={Config.googleId}
                              onSuccess={this.responseGoogle}
                              onFailure={this.responseGoogle}
                              className="btn-google"
                          >
                              <FontAwesome
                                  name='google'
                              />
                              <span> Login dengan Google</span>
                          </GoogleLogin>
                      </center>
                      <p className={classes.divider}>ATAU</p>
                        <FormItem
                            label="Email"
                            validateStatus={emailField}
                            help={emailLabel}
                        >
                            <Input onKeyUp={(e) => this.setStateIni(e, 'email')} />
                        </FormItem>
                        <FormItem
                            label="Password"
                            validateStatus={passwordField}
                            help={passwordLabel}
                        >
                            <Input type="password" onKeyUp={(e) => this.setStateIni(e, 'password')} />
                        </FormItem>
                        <p>Belum punya akun ? Daftar <Link to="/daftar">Disini</Link></p>
                    </CardBody>
                    <CardFooter className={classes.cardFooter}>
                      <Button color="warning" size="lg" onClick={this.sendLogin}>
                        Login
                      </Button>
                    </CardFooter>
                  </form>
                </Card>
              </GridItem>
            </GridContainer>
          </div>
          <Footer whiteFont />
        </div>
      </div>
    );
  }
}

export default withStyles(loginPageStyle)(LoginPage);
