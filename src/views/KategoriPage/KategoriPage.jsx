import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import styles from "assets/jss/material-kit-react/views/components.jsx";

// Components
import HeaderMenu from '../General/HeaderMenu';
import Footer from '../General/Footer';
import KategoriContent from './KategoriContent';

import image from "assets/img/header.jpg";

function ButtonBases(props) {
    //const { classes } = props;

    return (
        <div>
            <div className="wrapper-navbar content" style={{backgroundImage: "url(" + image + ")", height: "200px", backgroundPosition: "0px 70px"}}>
                <HeaderMenu/>
                <br />
                <h1 align="center">Kategori Warung</h1>
            </div>
            <KategoriContent/>
            <Footer/>
        </div>
    );
}

ButtonBases.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ButtonBases);