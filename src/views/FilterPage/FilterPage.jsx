import React from "react";
import Config from "../../config";
import String from "../../string";
// material-ui components
import withStyles from "material-ui/styles/withStyles";
import HeaderMenu from "../General/HeaderMenu";
import HeaderForm from "../HomePage/HeaderForm";
import FilterItem from "./FilterItem";

import loginPageStyle from "assets/jss/material-kit-react/views/loginPage.jsx";

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        let keywords = props.match.params.keywords;
        let price = props.match.params.price;
        let time = props.match.params.time;
        let dayAll = props.match.params.day;
        let kategoriMenuAll = props.match.params.kategori;
        let filterStatus = props.match.params.filterStatus;
        let priceMin = Config.priceMin;
        let priceMax = Config.priceMax;
        let openTime = Config.openTime;
        let closeTime = Config.closeTime;
        let daySelect = [];
        let kategoriMenuSelect = [];
        //let offset = 0;


        if(price !== undefined) {
            price = price.split('-');
            priceMin = price[0];
            priceMax = price[1];
        }

        if(time !== undefined) {
            let timeArr = time.split('-');
            openTime = timeArr[0];
            closeTime = timeArr[1];
        }

        if(kategoriMenuAll !== undefined) {
            if(kategoriMenuAll.includes(',')) {
                kategoriMenuSelect = kategoriMenuAll.split(',');
            } else {
                kategoriMenuSelect[0] = kategoriMenuAll;
            }
        } else {
            kategoriMenuSelect = String.kategoriMenuSelect;
        }

        if(dayAll !== undefined) {
            if(dayAll.includes(',')) {
                let daySelectIni = dayAll.split(',');
                for(var key in daySelectIni) {
                    daySelect[key] = parseInt(daySelectIni[key], 10);
                }
            } else {
                daySelect[0] = dayAll;
            }
        } else {
            daySelect = String.dayListSelect;
        }

        this.state = {
            keywords: keywords,
            priceMin: priceMin,
            priceMax: priceMax,
            openTime: openTime,
            closeTime: closeTime,
            daySelect: daySelect,
            kategoriMenuSelect: kategoriMenuSelect,
            filterStatus: filterStatus,
            offset: 0,
            hasMoreItems: true
        };
    }
    componentDidMount() {

    }
    render() {
        //const { classes, ...rest } = this.props;
        //const { classes } = this.props;

        return (
            <div>
                <HeaderMenu/>
                <br /><br /><br /><br />
                <HeaderForm {...this.state} />
                <FilterItem {...this.state}/>
            </div>
        );
    }
}

export default withStyles(loginPageStyle)(LoginPage);
