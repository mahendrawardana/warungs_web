import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// material-ui components
import withStyles from "material-ui/styles/withStyles";
import Button from "components/CustomButtons/Button.jsx";
// @material-ui/icons
import HeaderMenu from "../General/HeaderMenu";
// core components
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Parallax from "components/Parallax/Parallax.jsx";
import {reactLocalStorage} from 'reactjs-localstorage';
import { message, Input, Form } from 'antd';
import { Input as InputStrap } from 'reactstrap';
import axios from "axios";

import profilePageStyle from "assets/jss/material-kit-react/views/profilePage.jsx";

const FormItem = Form.Item;

class WarungkuPage extends React.Component {

    constructor(props) {
        super(props);
        let user  = reactLocalStorage.getObject('user');
        this.state = {
            name: user.name,
            email: user.email,
            phone: user.phone,
            token: user.token,
            thumbnail: user.thumbnail,
            form: []
        };
    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    setStateIni = (event, field) => {
        var stateObject = function() {
            let returnObj = {};
            returnObj[field] = event.target.value;
            return returnObj;
        }.bind(event)();
        this.setState(stateObject);
    }

    onChange(e) {
        this.setState({
            thumbnailUpload: e.target.files[0]
        });
    }

    sendUpdate = () => {
        const self = this;
        var headers = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': this.state.token
                }
        }

        const formData = new FormData();
        formData.append('thumbnail', this.state.thumbnailUpload);
        formData.append('name', this.state.name);
        formData.append('email', this.state.email);
        formData.append('phone', this.state.phone);
        if(this.state.password) {
            formData.append('password', this.state.password);
        }

        axios.post('/user/profile', formData, headers)
            .then(function(response) {

                message.config({
                    top: 100,
                    duration: 2,
                    maxCount: 3,
                });

                let resp = response.data;
                if(!resp.success) {
                    const form_self = resp.data;
                    self.setState({
                        form: form_self
                    });

                    console.log(form_self);

                    message.error(resp.message, 2);
                } else {
                    self.setState({
                        form: [],
                        user: resp.data
                    });
                    reactLocalStorage.setObject('user', resp.data);
                    message.success(resp.message, 2);
                    window.location.reload();
                }
            })
            .catch(function (error) {
                console.log(error);
            });

    }


  render() {
    //const { classes, ...rest } = this.props;
    const { classes } = this.props;
    const { form } = this.state;
    const imageClasses = classNames(
      classes.imgRaised,
      classes.imgRoundedCircle,
      classes.imgFluid
    );

      const nameField = form['name'] !== undefined ? 'error' : '';
      const nameLabel = nameField === "error" ? form['name'] : '';
      const emailField = form['email'] !== undefined ? 'error' : '';
      const emailLabel = emailField === "error" ? form['email'] : '';
      const phoneField = form['phone'] !== undefined ? 'error' : '';
      const phoneLabel = phoneField === "error" ? form['phone'] : '';
      const thumbnailField = form['thumbnail'] !== undefined ? 'error' : '';
      const thumbnailLabel = thumbnailField === "error" ? form['thumbnail'] : '';
      const passwordField = form['password'] !== undefined ? 'error' : '';
      const passwordLabel = passwordField === "error" ? form['password'] : '';

    return (
      <div>
        <HeaderMenu/>
        <Parallax small filter image={require("assets/img/header.jpg")} />
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div>
            <div className={classes.container}>
              <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={6}>
                  <div className={classes.profile}>
                    <div>
                      <img src={this.state.thumbnail} alt={this.state.name} className={imageClasses} />
                    </div>
                    <div className={classes.name}>
                      <h3 className={classes.title}>{this.state.name}</h3>
                      <h6>{this.state.email}</h6>
                    </div>
                  </div>
                </GridItem>
              </GridContainer>
              <GridContainer justify="center" className="profile-wrapper">
                <GridItem xs={12} sm={12} md={7} className={classes.navWrapper}>
                    <FormItem
                        label="Nama"
                        validateStatus={nameField}
                        help={nameLabel}
                    >
                        <Input onKeyUp={(e) => this.setStateIni(e, 'name')} defaultValue={this.state.name} />
                    </FormItem>
                    <FormItem
                        label="Email"
                        validateStatus={emailField}
                        help={emailLabel}
                    >
                        <Input onKeyUp={(e) => this.setStateIni(e, 'email')} defaultValue={this.state.email} />
                    </FormItem>
                    <FormItem
                        label="Telepon"
                        validateStatus={phoneField}
                        help={phoneLabel}
                    >
                        <Input onKeyUp={(e) => this.setStateIni(e, 'phone')} defaultValue={this.state.phone} />
                    </FormItem>
                    <FormItem
                        label="Avatar"
                        validateStatus={thumbnailField}
                        help={thumbnailLabel}
                    >
                        <InputStrap type="file" name="thumbnail" id="exampleFile" onChange={(e) => this.onChange(e)} />
                    </FormItem>
                    <FormItem
                        label="Password"
                        validateStatus={passwordField}
                        help={passwordLabel}
                    >
                        <Input type="password" onKeyUp={(e) => this.setStateIni(e, 'password')} />
                    </FormItem>
                    <br /><br />
                    <Button color="warning" size="lg" onClick={this.sendUpdate}>
                        Perbarui Profil
                    </Button>
                </GridItem>
              </GridContainer>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default withStyles(profilePageStyle)(WarungkuPage);
