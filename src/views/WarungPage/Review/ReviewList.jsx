import React from 'react';
import Axios from 'axios';
import Config from '../../../config';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import {
    InputGroup,
    InputGroupAddon,
    InputGroupButtonDropdown,
    InputGroupDropdown,
    Input,
    Button,
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
   } from 'reactstrap';

//Bootstrap
import { Container, Row, Col } from "reactstrap";
import { reactLocalStorage } from '../../../../node_modules/reactjs-localstorage';

Axios.defaults.baseURL = Config.API_URL;
//axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

const Search = Input.Search;
const styles = theme => ({
    root: {
        flexGrow: 1,
        maxWidth: "100%",
    },
    demo: {
        backgroundColor: theme.palette.background.paper,
    },
    title: {
        margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`,
    },
});

class ReviewList extends React.Component {

    constructor(props) {
        super(props);

        const user = reactLocalStorage.getObject('user');
        const token = reactLocalStorage.get('token');
        const statusLogin = reactLocalStorage.get('statusLogin');

        this.submitReview = this.submitReview.bind(this);

        this.state = {
            dense: false,
            secondary: true,
            review: '',
            user: user,
            token: token,
            statusLogin: statusLogin,
            reviewList: []
        };
    }

    componentDidMount() {
        console.log(this.props);
    }

    setStateIni = (event, field) => {
        var stateObject = function() {
            let returnObj = {};
            returnObj[field] = event.target.value;
            return returnObj;
        }.bind(event)();

        this.setState(stateObject);
    }

    submitReview(warungId) {
        let review = this.state.review;
        let token = this.state.token;
        const reviewList = this.state.reviewList;
        let data = {
            warungId: warungId,
            review: review
        };
        var headers = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            }
        }

        console.log(reviewList);

        // Axios.post('warung/review', data, headers)
        //     .then(function(response) {
        //         let resp = response.data;
        //         if(!resp.success) {
        //             window.location.reload();
        //         } else {
                    
        //         }
        //     })
        //     .catch(function (error) {
        //         console.log(error);
        //     });
    }

    render() {
        const { classes } = this.props;
        const { dense, secondary } = this.state;
        const reviewsList = this.props.reviewList;
        const warungId = this.props.warungId;
        const formReview = [];
        let statusLogin = this.state.statusLogin;

        const ReviewsItem = [];

        for (var key in reviewsList) {
            const data = reviewsList[key];
            const user_name = data.user_name === '' ? '-' : data.user_name;
            ReviewsItem.push(
                <ListItem key={key}>
                    <ListItemAvatar>
                        <img src={data.user_thumbnail} style={{borderRadius: "50%", width: "50px", height: "50px"}} alt={user_name} />
                    </ListItemAvatar>
                    <ListItemText
                        primary={data.user_name}
                        secondary={data.review}
                    />
                    <ListItemSecondaryAction>
                        {data.time}
                    </ListItemSecondaryAction>
                </ListItem>
            )
        }

        if(statusLogin === 'yes') {
            formReview.push(
                <div>
                    <br />
                        <InputGroup>
                            <Input onKeyUp={(e) => this.setStateIni(e, 'review')} />
                            <InputGroupAddon addonType="prepend" onClick={() => this.submitReview(warungId)}><Button>Kirim Review</Button></InputGroupAddon>
                        </InputGroup>
                </div>
            );
        }

        return (
            <Container>
                <Row>
                    <Col xs={12} sm={{size: 8, offset: 2}}>
                        <div className={classes.root}>
                            <Grid container spacing={16}>
                                <Grid item xs={12} md={12}>
                                    <div className={classes.demo}>
                                        <List dense={dense}>
                                            {ReviewsItem}
                                        </List>
                                    </div>
                                </Grid>
                            </Grid>
                        </div>
                        {formReview}
                    </Col>
                </Row>
            </Container>
        );
    }
}

ReviewList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ReviewList);