import React from 'react';
import Axios from 'axios';
import Async from 'async';
import Config from '../../../config';
import {reactLocalStorage} from 'reactjs-localstorage';

import styles from "assets/jss/material-kit-react/views/components.jsx";


import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import { Container, Row, Col } from "reactstrap";

// Components
import ReviewList from './ReviewList';
import HeaderMenu from '../../General/HeaderMenu';
import WarungSlider from '../WarungSlider';
import WarungMenu from '../WarungMenu';
import Footer from '../../General/Footer';


import {
    InputGroup,
    InputGroupAddon,
    InputGroupButtonDropdown,
    InputGroupDropdown,
    Input,
    Button,
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
   } from 'reactstrap';

Axios.defaults.baseURL = Config.API_URL;
//axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';


class WarungLokasi extends React.Component {

    constructor(props) {
        super(props);

        this.submitReview = this.submitReview.bind(this);

        let user = reactLocalStorage.getObject('user');
        let statusLogin = reactLocalStorage.get('statusLogin');
        let token = reactLocalStorage.get('token');

        this.state = {
            permalink: props.match.params.permalink,
            warungRow: [],
            warungId: '',
            warungSlider: [],
            reviewList: [],
            user: user,
            statusLogin: statusLogin,
            token: token,
            dense: false,
            secondary: true,
        }
    }


    componentDidMount() {
        const permalink = this.state.permalink;
        const ini = this;

        function postWarungRow(callback) {
            Axios({
                url: '/warung/row',
                method: 'post',
                data: {
                    'field': 'permalink',
                    'value': permalink
                }
            }).then((response) => {
                console.log(response);
                ini.setState({
                    warungRow: response.data.data,
                    warungId: response.data.data.id
                });
                callback(null, 'postWarungRow');
            }).catch(function(error) {
                console.log(error);
            });
        }

        function getWarungSlider(callback) {
            Axios({
                url: '/warung/slider/'+ini.state.warungRow.id,
                method: 'get'
            }).then((response) => {
                ini.setState({
                    warungSlider: response.data.data
                });
                callback(null, 'getSlider');
            }).catch((error) => {
                console.log(error);
            });
        }

        function getReviews(callback) {
            const warungId = ini.state.warungRow.id;

            Axios({
                url: '/warung/review/'+warungId,
                method: 'get',
            }).then((response) => {
                ini.setState({
                    reviewList: response.data.data
                });
                callback(null, 'getReviews');
            }).catch((error) => {
                console.log(error);
            });
        }

        Async.series([
            postWarungRow,
            getWarungSlider,
            getReviews
        ], (err, results) => {
            //console.log(results);
        });
    }

    setStateIni = (event, field) => {
        var stateObject = function() {
            let returnObj = {};
            returnObj[field] = event.target.value;
            return returnObj;
        }.bind(event)();

        this.setState(stateObject);
    }

    submitReview(warungId) {
        let review = this.state.review;
        let token = this.state.token;
        let user = this.state.user;
        const reviewList = this.state.reviewList;
        let data = {
            warungId: warungId,
            review: review
        };
        var headers = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            }
        }

        Axios.post('/warung/review', data, headers)
             .then((response) => {
                let data = response.data.data;
                reviewList.push(data);
                this.setState({
                    reviewList: reviewList
                });
             })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {

        const { classes } = this.props;
        const { dense, secondary } = this.state;
        const permalink = this.state.permalink;
        const reviewList = this.state.reviewList;
        const warungSlider = this.state.warungSlider;
        const warungRow = this.state.warungRow;
        let statusLogin = this.state.statusLogin;
        let warungId = this.state.warungId;
        //const warungRow = this.state.warungRow;

        const ReviewsItem = [];
        const formReview = [];

        for (var key in reviewList) {
            const data = reviewList[key];
            const user_name = data.user_name === '' ? '-' : data.user_name;
            ReviewsItem.push(
                <ListItem key={key}>
                    <ListItemAvatar>
                        <img src={data.user_thumbnail} style={{borderRadius: "50%", width: "50px", height: "50px"}} alt={user_name} />
                    </ListItemAvatar>
                    <ListItemText
                        primary={data.user_name}
                        secondary={data.review}
                    />
                    <ListItemSecondaryAction>
                        {data.time}
                    </ListItemSecondaryAction>
                </ListItem>
            )
        }

        if(statusLogin === 'yes') {
            formReview.push(
                <div>
                    <br />
                        <InputGroup>
                            <Input onKeyUp={(e) => this.setStateIni(e, 'review')} />
                            <InputGroupAddon addonType="prepend" onClick={() => this.submitReview(warungId)}><Button>Kirim Review</Button></InputGroupAddon>
                        </InputGroup>
                </div>
            );
        }

        return (
            <div>
                <HeaderMenu/>
                <WarungSlider slider={warungSlider}/>
                <WarungMenu permalink={permalink}/>
                <br /><br />
                {/* <ReviewList reviewList={reviewList} warungRow={warungRow} warungId={warungRow.id}/> */}

                <Container>
                    <Row>
                        <Col xs={12} sm={{size: 8, offset: 2}}>
                            <div className={classes.root}>
                                <Grid container spacing={16}>
                                    <Grid item xs={12} md={12}>
                                        <div className={classes.demo}>
                                            <List dense={dense}>
                                                {ReviewsItem}
                                            </List>
                                        </div>
                                    </Grid>
                                </Grid>
                            </div>
                            {formReview}
                        </Col>
                    </Row>
                </Container>

                <Footer/>
            </div>
        );
    }
}


export default withStyles(styles)(WarungLokasi);