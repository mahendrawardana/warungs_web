import React from 'react';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import styles from "assets/jss/material-kit-react/views/components.jsx";

// Components
import HeaderMenu from '../General/HeaderMenu';
import Footer from '../General/Footer';
import WarungSlider from './WarungSlider';
import WarungMenu from './WarungMenu';
import WarungPhone from './WarungPhone';

function ButtonBases(props) {
    const { classes } = props;

    return (
        <div>
            <HeaderMenu/>
            <WarungSlider/>
            <WarungMenu/>
            <WarungPhone/>
            <Footer/>
        </div>
    );
}

ButtonBases.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ButtonBases);