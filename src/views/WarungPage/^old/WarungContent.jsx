import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

// Bootstrap
import { Container, Row, Col } from 'reactstrap';

const styles = {
    card: {
        maxWidth: 345,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
};

class WarungContent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            warungMenu: []
        };
    }

    render() {
        const { classes } = this.props;
        const warungMenu = this.props.warungMenu;
        const menuList = [];

        for(var key in warungMenu) {
            const data = warungMenu[key];
            menuList.push(
                <Col xs={12} sm={3} key={key}>
                    <Card className={classes.card}>
                        <CardMedia
                            className={classes.media}
                            image={data.thumbnail}
                            title={data.name}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="headline" component="h2">
                                {data.name}
                            </Typography>
                            <Typography component="p">
                                {data.price}
                            </Typography>
                        </CardContent>
                    </Card>
                </Col>
            );
        }

        return (
            <div>
                <Container>
                    <br />
                    <Row className={"warung-menu-list"}>
                        {menuList}
                    </Row>
                </Container>
            </div>
        );
    }
}

export default withStyles(styles)(WarungContent);