import React from 'react';
import Config from '../../config';
import async from 'async';
import series from 'async/series';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import axios from 'axios';

import styles from "assets/jss/material-kit-react/views/components.jsx";


// Bootstrap
import { Container, Row, Col } from 'reactstrap';

// Components
import HeaderMenu from '../General/HeaderMenu';
import Footer from '../General/Footer';
import WarungSlider from './WarungSlider';
import WarungMenu from './WarungMenu';
import WarungContent from './WarungContent';
import WarungPhone from './WarungPhone';
import WarungMap from './WarungMap';

axios.defaults.baseURL = Config.API_URL;
//axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

class WarungPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            permalink: props.match.params.permalink,
            menu: props.match.params.menu,
            warungRow: [],
            warungSlider: [],
            warungMenu: [],
            warungReview: [],
            content: []
        };
    }

    componentDidMount() {
        let permalink = this.state.permalink;
        const ini = this;
        const menu = this.state.menu;

        console.log(this.props.location.pathname);

        function getWarungRow(callback) {
            axios({
                url: '/warung/row',
                method:'post',
                data: {
                    'field': 'permalink',
                    'value': permalink
                }
            }).then((response) => {
                ini.setState({
                    warungRow: response.data.data
                });
                callback(null, 'one');
            })
            .catch(function(error) {
                console.log(error)
            });
        }

        function getWarungSlider(callback) {
            axios({
                url: '/warung/slider/'+ini.state.warungRow.id,
                method:'get'
            }).then((response) => {
                ini.setState({
                    warungSlider: response.data.data
                });
                callback(null, 'two');
            }).catch(function(error) {
                console.log(error)
            });
        }

        function getWarungMenu(callback) {
            axios({
                url: '/warung/menu/'+ini.state.warungRow.id,
                method:'get'
            }).then((response) => {
                ini.setState({
                    warungMenu: response.data.data
                });
                callback(null, 'three');
            }).catch(function(error) {
                console.log(error)
            });
        }

        function getWarungReview(callback) {
            axios({
                url: '/warung/review/'+ini.state.warungRow.id,
                method:'get'
            }).then((response) => {
                ini.setState({
                    warungReview: response.data.data
                });
                callback(null, 'four');
            }).catch(function(error) {
                console.log(error)
            });
        }

        function getContent(callback) {
            const content_new = [];

            if(menu == 'telepon') {
                content_new.push(
                    <WarungPhone warungRow={ini.state.warungRow} key={1}/>
                );
            } else if(menu == 'lokasi') {
                content_new.push(
                    <WarungMap warungRow={ini.state.warungRow} key={1}/>
                );
            } else {
                content_new.push(
                    <WarungContent warungMenu={ini.state.warungMenu} key={1}/>
                );
            }

            ini.setState({
                content: content_new
            });

            callback(null, 'five');
        }

        async.series([
            getWarungRow,
            getWarungSlider,
            getWarungMenu,
            getWarungReview,
            getContent
        ], function (err, results) {
            console.log(results);
        });

    }

    render() {

        const warungRow = this.state.warungRow;
        const warungMenu = this.state.warungMenu;
        const warungSlider = this.state.warungSlider;

        const content = this.state.content;

        return(
            <div>
                <HeaderMenu/>
                <WarungSlider slider={warungSlider}/>
                <WarungMenu warungRow={warungRow}/>
                <Container>
                    <Row>
                        <Col xs={12}>
                            <h1 align="center" className="warung-nama">{warungRow.name}</h1>
                            <p align="center">
                                {warungRow.description}
                            </p>
                        </Col>
                    </Row>
                </Container>
                {content}
                <Footer/>
            </div>
        )
    }

}

export default withStyles(styles)(WarungPage);