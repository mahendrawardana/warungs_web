import React from 'react';
import Config from '../../config';
import async from 'async';
import series from 'async/series';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import axios from 'axios';

import styles from "assets/jss/material-kit-react/views/components.jsx";


// Bootstrap
import { Container, Row, Col } from 'reactstrap';

// Components
import HeaderMenu from '../General/HeaderMenu';
import WarungSlider from './WarungSlider';
import WarungMenu from './WarungMenu';

axios.defaults.baseURL = Config.API_URL;
//axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

class WarungPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            warungRow: [],
            warungSlider: [],
            warungMenu: [],
            warungReview: []
        };
    }

    componentDidMount() {
        let permalink = this.props.permalink;
        const ini = this;

        function getWarungRow(callback) {
            axios({
                url: '/warung/row',
                method:'post',
                data: {
                    'field': 'permalink',
                    'value': permalink
                }
            }).then((response) => {
                ini.setState({
                    warungRow: response.data.data
                });
                callback(null, 'one');
            })
            .catch(function(error) {
                console.log(error)
            });
        }

        function getWarungSlider(callback) {
            axios({
                url: '/warung/slider/'+ini.state.warungRow.id,
                method:'get'
            }).then((response) => {
                ini.setState({
                    warungSlider: response.data.data
                });
                callback(null, 'two');
            }).catch(function(error) {
                console.log(error)
            });
        }

        function getWarungMenu(callback) {
            axios({
                url: '/warung/menu/'+ini.state.warungRow.id,
                method:'get'
            }).then((response) => {
                ini.setState({
                    warungMenu: response.data.data
                });
                callback(null, 'three');
            }).catch(function(error) {
                console.log(error)
            });
        }

        function getWarungReview(callback) {
            axios({
                url: '/warung/review/'+ini.state.warungRow.id,
                method:'get'
            }).then((response) => {
                ini.setState({
                    warungReview: response.data.data
                });
                callback(null, 'four');
            }).catch(function(error) {
                console.log(error)
            });
        }

        async.series([
            getWarungRow,
            getWarungSlider,
            getWarungMenu,
            getWarungReview
        ], function (err, results) {
            console.log(results);
        });

    }

    render() {

        const warungRow = this.state.warungRow;
        const warungMenu = this.state.warungMenu;
        const warungSlider = this.state.warungSlider;

        return(
            <div>
                <HeaderMenu/>
                <WarungSlider slider={warungSlider}/>
                <WarungMenu warungRow={warungRow}/>
                <Container>
                    <Row>
                        <Col xs={12}>
                            <h1 align="center" className="warung-nama">{warungRow.name}</h1>
                            <p align="center">
                                {warungRow.description}
                            </p>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }

}

export default withStyles(styles)(WarungPage);