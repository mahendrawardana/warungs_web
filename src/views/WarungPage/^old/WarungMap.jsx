/**
 * Created by Mahendra Wardana Z on 6/3/2018.
 */
import React from "react";

import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import { Container, Row, Col } from "reactstrap";

class WarungMap extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const warungRow = this.props.warungRow;


        const GoogleMapExample = withGoogleMap(props => (
            <GoogleMap
                defaultCenter={{ lat: warungRow.latitude, lng: warungRow.longitude }}
                defaultZoom = { 14 }
            >
                {props.isMarkerShown && <Marker position={{ lat: warungRow.latitude, lng: warungRow.latitude }} />}
            </GoogleMap>
        ));

        return (
            <Container>
                <br />
                <Row>
                    <Col xs={12} sm={4}>
                        Alamat : Jalan Pahlawan, Singaraja, Buleleng, Bali
                    </Col>
                    <Col xs={12} sm={8}>
                        <GoogleMapExample
                            isMarkerShown
                            containerElement={ <div style={{ height: `350px`, width: '100%' }} /> }
                            mapElement={ <div style={{ height: `100%` }} /> }
                        />
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default WarungMap;