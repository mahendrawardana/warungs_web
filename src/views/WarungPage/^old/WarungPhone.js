import React from "react";


// Ant D
import { Tag } from 'antd';

// Bootstrap
import { Container, Row, Col } from 'reactstrap';
import IconPhone from 'react-icons/lib/md/local-phone';
import IconSMS from 'react-icons/lib/md/mail-outline';
import IconWhatsApp from 'react-icons/lib/fa/whatsapp';

class WarungPhone extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const styleIcon = {
            fontSize: "40px"
        };
        const warungRow = this.props.warungRow;

        return (
            <Container>
                <Row>
                    <Col xs={12} sm={4} className="text-center">
                        <IconPhone style={styleIcon}/><br/>
                        <Tag color="#616161">Telepon</Tag><br />
                        {warungRow.phone}
                    </Col>
                    <Col xs={12} sm={4} className="text-center">
                        <IconSMS style={styleIcon}/><br/>
                        <Tag color="#039be5">SMS</Tag><br />
                        {warungRow.phone}
                    </Col>
                    <Col xs={12} sm={4} className="text-center">
                        <IconWhatsApp style={styleIcon}/><br/>
                        <Tag color="#25D366">WhatsApp</Tag><br />
                        {warungRow.phone}
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default WarungPhone;