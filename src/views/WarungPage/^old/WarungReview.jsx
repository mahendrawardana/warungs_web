import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import AccountCircle from '@material-ui/icons/AccountCircle';
import DeleteIcon from '@material-ui/icons/Delete';

//Bootstrap
import { Container, Row, Col } from "reactstrap";

const styles = theme => ({
    root: {
        flexGrow: 1,
        maxWidth: "100%",
    },
    demo: {
        backgroundColor: theme.palette.background.paper,
    },
    title: {
        margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`,
    },
});

function generate(element) {
    return [0, 1, 2].map(value =>
        React.cloneElement(element, {
            key: value,
        }),
    );
}

class InteractiveList extends React.Component {
    state = {
        dense: false,
        secondary: true,
    };

    render() {
        const { classes } = this.props;
        const { dense, secondary } = this.state;

        return (
            <Container>
                <Row>
                    <Col xs={12}>
                        <h1 align="center" className="warung-nama">Warung Buk Angga</h1>
                        <p align="center">
                            Warung adalah usaha kecil milik keluarga yang berbentuk kedai, kios, toko kecil, atau restoran sederhana — istilah "warung" dapat ditemukan di Indonesia dan Malaysia. Warung adalah salah satu bagian penting dalam kehidupan keseharian rakyat Indonesia.
                        </p>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={{size: 8, offset: 2}}>
                        <div className={classes.root}>
                            <Grid container spacing={16}>
                                <Grid item xs={12} md={12}>
                                    <div className={classes.demo}>
                                        <List dense={dense}>
                                            {generate(
                                                <ListItem>
                                                    <ListItemAvatar>
                                                        <Avatar>
                                                            <AccountCircle/>
                                                        </Avatar>
                                                    </ListItemAvatar>
                                                    <ListItemText
                                                        primary="Ngurah"
                                                        secondary="Enak Sekali makan disini, tak bisa dibayangkan ..."
                                                    />
                                                    <ListItemSecondaryAction>
                                                            2h
                                                    </ListItemSecondaryAction>
                                                </ListItem>,
                                            )}
                                        </List>
                                    </div>
                                </Grid>
                            </Grid>
                        </div>
                    </Col>
                </Row>
            </Container>
        );
    }
}

InteractiveList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(InteractiveList);