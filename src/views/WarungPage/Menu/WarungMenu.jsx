import React from 'react';
import Axios from 'axios';
import Async from 'async';
import Config from '../../../config';
import String from '../../../string';
import {reactLocalStorage} from 'reactjs-localstorage';

// Style
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { Avatar } from 'antd';

// Bootstrap
import { Container, Row, Col } from 'reactstrap';

// Components
import HeaderMenu from '../../General/HeaderMenu';
import WarungSlider from '../WarungSlider';
import WarungMenu from '../WarungMenu';
import WarungTitle from '../WarungTitle';
import Footer from '../../General/Footer';


Axios.defaults.baseURL = Config.API_URL;
//axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

const styles = {
    card: {
        maxWidth: 345,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
};


class WarungMenuPage extends React.Component {

    constructor(props) {
        super(props);
        let user = reactLocalStorage.getObject('user');

        this.state = {
            permalink: props.match.params.permalink,
            warungRow: [],
            warungSlider: [],
            warungMenu: [],
            owner: [],
            user: user,
            token: user.token
        }
    }

    componentWillMount() {
        const permalink = this.state.permalink;
        const ini = this;
        let token = this.state.token;

        function postWarungRow(callback) {
            Axios({
                url: '/warung/row',
                method: 'post',
                data: {
                    'field': 'permalink',
                    'value': permalink
                },
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': token
                }
            }).then((response) => {
                ini.setState({
                    warungRow: response.data.data,
                    owner: response.data.data.owner
                });
                callback(null, 'postWarungRow');
            }).catch(function(error) {
                console.log(error);
            });
        }

        function getWarungSlider(callback) {
            Axios({
                url: '/warung/slider/'+ini.state.warungRow.id,
                method: 'get'
            }).then((response) => {
                ini.setState({
                    warungSlider: response.data.data
                });
                callback(null, 'getSlider');
            }).catch((error) => {
                console.log(error);
            });
        }

        function getWarungMenu(callback) {
            Axios({
                url: '/warung/menu/'+ini.state.warungRow.id,
                method: 'get'
            }).then((response) => {
                ini.setState({
                    warungMenu: response.data.data
                });
                callback(null, 'getWarungMenu');
            }).catch((error) => {
                console.log(error);
            });
        }

        Async.series([
            postWarungRow,
            getWarungSlider,
            getWarungMenu
        ], (err, results) => {
            //console.log(results);
        });
    }

    render() {
        const { classes } = this.props;
        const warungRow = this.state.warungRow;
        const warungSlider = this.state.warungSlider;
        const permalink = this.state.permalink;
        const warungMenu = this.state.warungMenu;
        const owner = this.state.owner;
        const menuList = [];

        for(var key in warungMenu) {
            const data = warungMenu[key];
            menuList.push(
                <Col xs={12} sm={3} key={key}>
                    <Card className={classes.card}>
                        <CardMedia
                            className={classes.media}
                            image={data.thumbnail}
                            title={data.name}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="headline" component="h2">
                                {data.name}
                            </Typography>
                            <Typography component="p">
                                {data.price}
                            </Typography>
                        </CardContent>
                    </Card>
                </Col>
            );
        }

        return (
            <div>
                <HeaderMenu/>
                <WarungSlider slider={warungSlider}/>
                <WarungMenu permalink={permalink}/>
                <WarungTitle warungRow={warungRow} owner={owner}/>
                <Container>
                    <Row>
                        {menuList}
                    </Row>
                </Container>
                <Footer/>
            </div>
        );
    }
}

WarungMenuPage.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(WarungMenuPage);