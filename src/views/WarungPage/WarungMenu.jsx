/**
 * Created by Mahendra Wardana Z on 6/2/2018.
 */

import React from 'react';
import {Link} from 'react-router-dom';

// @material-ui/icons
import IconMenu from 'react-icons/lib/md/restaurant-menu';
import IconTelephone from 'react-icons/lib/md/contact-phone';
import IconLocation from 'react-icons/lib/md/location-on';
import IconComment from 'react-icons/lib/md/comment';

// Bootstrap
import { Container, Row, Col } from 'reactstrap';

class WarungMenu extends React.Component {
    render() {
        //const { classes } = this.props;
        const permalink = this.props.permalink;

        return (
            <div className="warung-menu">
                <Container>
                    <Row>
                        <Col xs={12}>
                            <ul>
                                <li>
                                    <Link to={"/warung/"+permalink}>
                                        <IconMenu /> Menu
                                    </Link>
                                </li>
                                <li>
                                    <Link to={"/warung/"+permalink+"/telepon"}>
                                        <IconTelephone/> Telepon
                                    </Link>
                                </li>
                                <li>
                                    <Link to={"/warung/"+permalink+"/lokasi"}>
                                        <IconLocation/> Lokasi
                                    </Link>
                                </li>
                                <li>
                                    <Link to={"/warung/"+permalink+"/review"}>
                                        <IconComment/> Review
                                    </Link>
                                </li>
                            </ul>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default WarungMenu;