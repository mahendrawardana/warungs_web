import React from 'react';
import Axios from 'axios';
import Async from 'async';
import Config from '../../../config';

import { withStyles } from '@material-ui/core/styles';
import styles from "assets/jss/material-kit-react/views/components.jsx";

import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Marker
} from "react-google-maps";
import { Container, Row, Col } from "reactstrap";
import { compose, withProps } from "recompose";


// Components
import HeaderMenu from '../../General/HeaderMenu';
import WarungSlider from '../WarungSlider';
import WarungMenu from '../WarungMenu';
import WarungTitle from '../WarungTitle';
import Footer from '../../General/Footer';


Axios.defaults.baseURL = Config.API_URL;
//axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';



class WarungLokasi extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            permalink: props.match.params.permalink,
            warungRow: [],
            warungSlider: [],
            owner: []
        }
    }


    componentDidMount() {
        const permalink = this.state.permalink;
        const ini = this;

        function postWarungRow(callback) {
            Axios({
                url: '/warung/row',
                method: 'post',
                data: {
                    'field': 'permalink',
                    'value': permalink
                }
            }).then((response) => {
                ini.setState({
                    warungRow: response.data.data,
                    owner: response.data.data.owner
                });
                callback(null, 'postWarungRow');
            }).catch(function(error) {
                console.log(error);
            });
        }

        function getWarungSlider(callback) {
            Axios({
                url: '/warung/slider/'+ini.state.warungRow.id,
                method: 'get'
            }).then((response) => {
                ini.setState({
                    warungSlider: response.data.data
                });
                callback(null, 'getSlider');
            }).catch((error) => {
                console.log(error);
            });
        }

        Async.series([
            postWarungRow,
            getWarungSlider
        ], (err, results) => {
            //console.log(results);
        });
    }

    render() {

        const warungRow = this.state.warungRow;
        const warungSlider = this.state.warungSlider;
        const permalink = this.state.permalink;
        const owner = this.state.owner;
        const latitude = parseFloat(warungRow.latitude);
        const longitude = parseFloat(warungRow.longitude);

        const MyMapComponent = compose(
            withProps({
                googleMapURL:
                "https://maps.googleapis.com/maps/api/js?key="+Config.keyMap+"&v=3.exp&libraries=geometry,drawing,places",
                loadingElement: <div style={{ height: `100%` }} />,
                containerElement: <div style={{ height: `400px` }} />,
                mapElement: <div style={{ height: `100%` }} />
            }),
            withScriptjs,
            withGoogleMap
        )(props => (
            <GoogleMap defaultZoom={5} defaultCenter={{ lat: -2.279866, lng: 117.369878 }}>
                {props.isMarkerShown && (
                    <Marker position={{ lat: latitude, lng: longitude }} />
                )}
            </GoogleMap>
        ));

        return (
        <div>
            <HeaderMenu/>
            <WarungSlider slider={warungSlider}/>
            <WarungMenu permalink={permalink}/>
            <WarungTitle warungRow={warungRow} owner={owner}/>
            <Container>
                <br />
                <Row>
                    <Col xs={12} sm={4}>
                        <strong>Alamat :</strong> Jalan Pahlawan, Singaraja, Buleleng, Bali
                    </Col>
                    <Col xs={12} sm={8}>

                        <MyMapComponent isMarkerShown />
                    </Col>
                </Row>
            </Container>
            <Footer/>
        </div>
        );
    }
}


export default withStyles(styles)(WarungLokasi);