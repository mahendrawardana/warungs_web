import React from 'react';

import {
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators,
    CarouselCaption
} from 'reactstrap';



class WarungSlider extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            activeIndex: 0,
            warungSlider: []
        };
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.goToIndex = this.goToIndex.bind(this);
        this.onExiting = this.onExiting.bind(this);
        this.onExited = this.onExited.bind(this);
    }

    componentDidMount() {

        this.setState({
            warungSlider: this.props.slider
        });

    }

    onExiting() {
        this.animating = true;
    }

    onExited() {
        this.animating = false;
    }

    next(slideItems) {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === slideItems.length - 1 ? 0 : this.state.activeIndex + 1;
        this.setState({ activeIndex: nextIndex });
    }

    previous(slideItems) {
        if (this.animating) return;
        const nextIndex = this.state.activeIndex === 0 ? slideItems.length - 1 : this.state.activeIndex - 1;
        this.setState({ activeIndex: nextIndex });
    }

    goToIndex(newIndex) {
        if (this.animating) return;
        this.setState({ activeIndex: newIndex });
    }


    render() {

        const { activeIndex } = this.state;
        const warungSlider = this.props.slider;
        const slides = [];
        const slideItems = [];
        let slidesCount = 0;
        let slidesControl;

        for (var key in warungSlider) {
            if (warungSlider.hasOwnProperty(key)) {
                const data = warungSlider[key];
                let description = data.description === null ? data.description : '-';
                let name = data.name === null ? data.name : '-';

                slides.push(
                    <CarouselItem
                        onExiting={this.onExiting}
                        onExited={this.onExited}
                        key={key}
                        style={{height: "300px !important"}}
                    >
                        <img src={data.file} alt={name} />
                        <CarouselCaption captionText={description} captionHeader={name} />
                    </CarouselItem>
                );

                slideItems[key] = {
                    src: data.file,
                    altText: data.description,
                    caption: data.name,
                };
                slidesCount = slidesCount + 1;
            }
        }

        if(slidesCount > 1) {
            slidesControl = (
                <div>
                    <CarouselControl direction="prev" directionText="Previous" onClickHandler={() => this.previous(slideItems)} />
                    <CarouselControl direction="next" directionText="Next" onClickHandler={() => this.next(slideItems)} />
                </div>
            );
        }


        return (
            <Carousel
                activeIndex={activeIndex}
                next={() => this.next(slideItems)}
                previous={() => this.previous(slideItems)}
                className={"warung-slider"}
            >
                <CarouselIndicators items={slideItems} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
                {slides}
                {slidesControl}
            </Carousel>
        );
    }
}


export default WarungSlider;
