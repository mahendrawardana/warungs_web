import React from 'react';
import Axios from 'axios';
import Async from 'async';
import Config from '../../../config';

// Style
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';


// Ant D
import { Tag } from 'antd';

// Bootstrap
import { Container, Row, Col } from 'reactstrap';
import IconPhone from 'react-icons/lib/md/local-phone';
import IconSMS from 'react-icons/lib/md/mail-outline';
import IconWhatsApp from 'react-icons/lib/fa/whatsapp';


// Components
import HeaderMenu from '../../General/HeaderMenu';
import WarungSlider from '../WarungSlider';
import WarungMenu from '../WarungMenu';
import WarungTitle from '../WarungTitle';
import Footer from '../../General/Footer';


Axios.defaults.baseURL = Config.API_URL;
//axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
Axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

const styles = {
    card: {
        maxWidth: 345,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
};


class WarungTelepon extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            permalink: props.match.params.permalink,
            warungRow: [],
            warungSlider: [],
            owner: []
        }
    }

    componentWillMount() {
        const permalink = this.state.permalink;
        const ini = this;

        function postWarungRow(callback) {
            Axios({
                url: '/warung/row',
                method: 'post',
                data: {
                    'field': 'permalink',
                    'value': permalink
                }
            }).then((response) => {
                ini.setState({
                    warungRow: response.data.data,
                    owner: response.data.data.owner
                });
                callback(null, 'postWarungRow');
            }).catch(function(error) {
                console.log(error);
            });
        }

        function getWarungSlider(callback) {
            Axios({
                url: '/warung/slider/'+ini.state.warungRow.id,
                method: 'get'
            }).then((response) => {
                ini.setState({
                    warungSlider: response.data.data
                });
                callback(null, 'getSlider');
            }).catch((error) => {
                console.log(error);
            });
        }

        Async.series([
            postWarungRow,
            getWarungSlider
        ], (err, results) => {
            //console.log(results);
        });
    }

    render() {
        //const { classes } = this.props;
        const warungRow = this.state.warungRow;
        const warungSlider = this.state.warungSlider;
        const permalink = this.state.permalink;
        const owner = this.state.owner;
        const styleIcon = {
            fontSize: "40px"
        };

        return (
            <div>
                <HeaderMenu/>
                <WarungSlider slider={warungSlider}/>
                <WarungMenu permalink={permalink}/>
                <WarungTitle warungRow={warungRow} owner={owner}/>
                <br />
                <Container>
                    <Row>
                        <Col xs={12} sm={4} className="text-center">
                            <IconPhone style={styleIcon}/><br/>
                            <Tag color="#616161">Telepon</Tag><br />
                            {warungRow.phone}
                        </Col>
                        <Col xs={12} sm={4} className="text-center">
                            <IconSMS style={styleIcon}/><br/>
                            <Tag color="#039be5">SMS</Tag><br />
                            {warungRow.phone}
                        </Col>
                        <Col xs={12} sm={4} className="text-center">
                            <IconWhatsApp style={styleIcon}/><br/>
                            <Tag color="#25D366">WhatsApp</Tag><br />
                            {warungRow.phone}
                        </Col>
                    </Row>
                </Container>
                <Footer/>
            </div>
        );
    }
}

WarungTelepon.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(WarungTelepon);