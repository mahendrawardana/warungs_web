import React from 'react';
import String from '../../string';

import {Avatar} from 'antd';
// Bootstrap
import {Container, Row, Col} from 'reactstrap';

import {reactLocalStorage} from "reactjs-localstorage";

class WarungTitle extends React.Component {

    constructor(props) {
        super(props);
        let user = reactLocalStorage.getObject('user');

        this.state = {
            warungRow: [],
            owner: [],
            user: user
        };
    }

    render() {
        const warungRow = this.props.warungRow;
        const owner = this.props.owner;

        return(
            <Container>
                <Row>
                    <Col xs={12}>
                        <h1 align="center" className="warung-nama">{warungRow.name}</h1>
                        <p align="center">
                            {warungRow.description}
                        </p>
                    </Col>
                    <Col xs={12} className="text-center">
                        <Avatar src={owner.ownerThumbnail} size="large" />
                        <h5 style={{ marginTop: "10px", marginBottom: "40px" }}>{owner.ownerNama}</h5>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default WarungTitle;