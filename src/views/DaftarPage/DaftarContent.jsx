import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ButtonBase from '@material-ui/core/ButtonBase';
import Typography from '@material-ui/core/Typography';

// Bootstrap
import { Container, Row, Col } from 'reactstrap';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        minWidth: 300,
        width: '100%',
    },
    image: {
        position: 'relative',
        height: 200,
        [theme.breakpoints.down('xs')]: {
            width: '100% !important', // Overrides inline-style
            height: 100,
        },
        '&:hover, &$focusVisible': {
            zIndex: 1,
            '& $imageBackdrop': {
                opacity: 0.15,
            },
            '& $imageMarked': {
                opacity: 0,
            },
            '& $imageTitle': {
                border: '4px solid currentColor',
            },
        },
    },
    focusVisible: {},
    imageButton: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        color: theme.palette.common.white,
    },
    imageSrc: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        backgroundSize: 'cover',
        backgroundPosition: 'center 40%',
    },
    imageBackdrop: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        backgroundColor: theme.palette.common.black,
        opacity: 0.4,
        transition: theme.transitions.create('opacity'),
    },
    imageTitle: {
        position: 'relative',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 4}px ${theme.spacing.unit + 6}px`,
    },
    imageMarked: {
        height: 3,
        width: 18,
        backgroundColor: theme.palette.common.white,
        position: 'absolute',
        bottom: -2,
        left: 'calc(50% - 9px)',
        transition: theme.transitions.create('opacity'),
    },
});

const images = [
    {
        url: 'https://upload.wikimedia.org/wikipedia/commons/2/2c/Made%27s_Warung_Nasi_Campur.jpg',
        title: 'Warung Nasi Campur',
        width: '33%',
    },
    {
        url: 'https://assets-a2.kompasiana.com/statics/crawl/5560895f0423bd271f8b4567.jpeg?t=o&v=760',
        title: 'Warung Babi Guling',
        width: '34%',
    },
    {
        url: 'https://asset.kompas.com/data/photo/2015/09/15/1604175warung-bakarr780x390.jpg',
        title: 'Warung Ikan Bakar',
        width: '33%',
    },
    {
        url: 'https://d3t543lkaz1xy.cloudfront.net/photo/5a57346b59cf0f3c1ba00308_m',
        title: 'Warung Nasi Uduk',
        width: '33%',
    },
    {
        url: 'http://1.bp.blogspot.com/-oTML_Tdt2FE/UkkzLSkFLcI/AAAAAAAAC8E/S86me9CmMOU/s1600/Bisnis+Warung+Lalapan.jpg',
        title: 'Warung Lalapan',
        width: '34%',
    },
    {
        url: 'http://gudegyudjumpusat.com/wp-content/uploads/2015/12/Nasi-Gudeg-Telur-Tahu-Tempe.jpg',
        title: 'Warung Tahu Tempe',
        width: '33%',
    },
    {
        url: 'https://mknmana.files.wordpress.com/2013/12/ayam-bakar-warung-tisya.jpg',
        title: 'Warung Ayam Bakar',
        width: '33%',
    },
    {
        url: 'https://pecelkembangturi.files.wordpress.com/2013/05/kedai-surawung6.jpg',
        title: 'Warung Makanan Laut',
        width: '34%',
    },
    {
        url: 'https://cdn.hellosehat.com/wp-content/uploads/2017/07/sate-kambing.jpg?x54339',
        title: 'Warung Makanan Kambing',
        width: '33%',
    },
];

function ButtonBases(props) {
    const { classes } = props;

    return (
        <Container>
            <Row>
                <Col xs="12">
                    <div className={classes.root}>
                        {images.map(image => (
                            <ButtonBase
                                focusRipple
                                key={image.title}
                                className={classes.image}
                                focusVisibleClassName={classes.focusVisible}
                                style={{
                                    width: image.width,
                                }}
                            >
                              <span
                                  className={classes.imageSrc}
                                  style={{
                                      backgroundImage: 'url('+image.url+')',
                                  }}
                              />
                                            <span className={classes.imageBackdrop} />
                                            <span className={classes.imageButton}>
                                <Typography
                                    component="span"
                                    variant="subheading"
                                    color="inherit"
                                    className={classes.imageTitle}
                                >
                                  {image.title}
                                    <span className={classes.imageMarked} />
                                </Typography>
                              </span>
                        </ButtonBase>
                    ))}
                </div>
                </Col>
            </Row>
        </Container>
    );
}

ButtonBases.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ButtonBases);