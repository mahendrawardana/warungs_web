import React from "react";
// material-ui components
import withStyles from "material-ui/styles/withStyles";
import axios from 'axios';
import { Redirect } from 'react-router-dom';
// core components
import Header from "components/Header/Header.jsx";
import HeaderLinks from "./DaftarHeaderLinks";
import Footer from "./DaftarFooter.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import { message, Input, Form } from 'antd';

import loginPageStyle from "assets/jss/material-kit-react/views/loginPage.jsx";

import image from "assets/img/background-register.jpg";


const FormItem = Form.Item;

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        // we use this to make the card to appear after the page has been rendered
        this.state = {
            cardAnimaton: "cardHidden",
            form: [],
            redirectStatus: false,
            redirectPath: ''
        };
    }
    componentDidMount() {
        // we add a hidden class to the card and after 700 ms we delete it and the transition appears
        setTimeout(
            function() {
                this.setState({ cardAnimaton: "" });
            }.bind(this),
            700
        );
    }

    sendRegister = () => {
        const self = this;
        message.loading('Sedang Mengirim Data ...', 1);
        axios.post('/user/register', {
            name: this.state.name,
            email: this.state.email,
            phone: this.state.phone,
            password: this.state.password,
            conf_password: this.state.conf_password,
        })
        .then(function (response) {
           // console.log(response.data);
            let resp = response.data;
            if(!resp.success) {
                message.error(resp.message, 3);
                const form_self = resp.data;
                self.setState({
                    form: form_self
                });
            } else {
                message.success(resp.message, 3, () => {
                    self.setState({
                        redirectStatus: true,
                        redirectPath: '/login'
                    });
                });
            }
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    setStateIni = (event, field) => {
        var stateObject = function() {
            let returnObj = {};
            returnObj[field] = event.target.value;
            return returnObj;
        }.bind(event)();

        this.setState(stateObject);
    }

    renderRedirect = () => {
        if(this.state.redirectStatus) {
            return <Redirect to={this.state.redirectPath}/>
        }
    }

    render() {
        const { classes, ...rest } = this.props;
        const { form } = this.state;

        const nameField = form['name'] !== undefined ? 'error' : '';
        const nameLabel = nameField === "error" ? form['name'] : '';
        const emailField = form['email'] !== undefined ? 'error' : '';
        const emailLabel = emailField === "error" ? form['email'] : '';
        const phoneField = form['phone'] !== undefined ? 'error' : '';
        const phoneLabel = phoneField === "error" ? form['phone'] : '';
        const passwordField = form['password'] !== undefined ? 'error' : '';
        const passwordLabel = passwordField === "error" ? form['password'] : '';
        const confPasswordField = form['conf_password'] !== undefined ? 'error' : '';
        const confPasswordLabel = confPasswordField === "error" ? form['conf_password'] : '';


        return (
            <div>
                { this.renderRedirect() }
                <Header
                    absolute
                    color="transparent"
                    brand="WARUNGS"
                    rightLinks={<HeaderLinks />}
                    {...rest}
                />
                <div
                    className={classes.pageHeader}
                    style={{
                        backgroundImage: "url(" + image + ")",
                        backgroundSize: "cover",
                        backgroundPosition: "top center"
                    }}
                >
                    <div className={classes.container}>
                        <GridContainer justify="center">
                            <GridItem xs={12} sm={12} md={6}>
                                <Card className={classes[this.state.cardAnimaton]}>
                                    <form className={classes.form}>
                                        <CardHeader color="warning" className={classes.cardHeader}>
                                            <h4>Daftar</h4>
                                        </CardHeader>
                                        <CardBody>
                                            <FormItem
                                                label="Nama"
                                                validateStatus={nameField}
                                                help={nameLabel}
                                            >
                                                <Input onKeyUp={(e) => this.setStateIni(e, 'name')} />
                                            </FormItem>
                                            <FormItem
                                                label="Email"
                                                validateStatus={emailField}
                                                help={emailLabel}
                                            >
                                                <Input onKeyUp={(e) => this.setStateIni(e, 'email')} />
                                            </FormItem>
                                            <FormItem
                                                label="Telepon/HP"
                                                validateStatus={phoneField}
                                                help={phoneLabel}
                                            >
                                                <Input onKeyUp={(e) => this.setStateIni(e, 'phone')} />
                                            </FormItem>
                                            <FormItem
                                                label="Password"
                                                validateStatus={passwordField}
                                                help={passwordLabel}
                                            >
                                                <Input type="password" onKeyUp={(e) => this.setStateIni(e, 'password')} />
                                            </FormItem>
                                            <FormItem
                                                label="Konfirmasi Password"
                                                validateStatus={confPasswordField}
                                                help={confPasswordLabel}
                                            >
                                                <Input type="password" onKeyUp={(e) => this.setStateIni(e, 'conf_password')} />
                                            </FormItem>
                                        </CardBody>
                                        <CardFooter className={classes.cardFooter}>
                                            <Button color="warning" size="lg" onClick={this.sendRegister}>
                                                Daftar
                                            </Button>
                                        </CardFooter>
                                    </form>
                                </Card>
                            </GridItem>
                        </GridContainer>
                    </div>
                    <Footer whiteFont />
                </div>
            </div>
        );
    }
}

export default withStyles(loginPageStyle)(LoginPage);
