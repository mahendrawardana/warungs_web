import React from "react";
// material-ui components
import withStyles from "material-ui/styles/withStyles";
// core components
import Card from "components/Card/Card.jsx";

import imagesStyles from "assets/jss/material-kit-react/imagesStyles.jsx";

import { cardTitle } from "assets/jss/material-kit-react.jsx";


import work5 from "assets/img/bg7.jpg";

const style = {
    ...imagesStyles,
    cardTitle,
};

class Cards extends React.Component {
    render() {
        const { classes } = this.props;
        return(
            <Card>
                <img className={classes.imgCard} src={work5} alt="Card-img" />
                <div className={classes.imgCardOverlay}>
                    <h4 className={classes.cardTitle}>Card title</h4>
                    <p>This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                    <p>Last updated 3 mins ago</p>
                </div>
            </Card>
        );
    }
};

export default withStyles(style)(Cards);