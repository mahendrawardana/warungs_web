import React from "react";
import moment from 'moment';
import Config from '../../config';
import String from '../../string';
// nodejs library that concatenates classes
import classNames from "classnames";
// material-ui components
import withStyles from "material-ui/styles/withStyles";
import Button from "components/CustomButtons/Button.jsx";
// @material-ui/icons
import HeaderMenu from "../General/HeaderMenu";
// core components
import Footer from "components/Footer/Footer.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Parallax from "components/Parallax/Parallax.jsx";
import {reactLocalStorage} from 'reactjs-localstorage';
import { message, Input, Form, Slider, Modal, Checkbox, TimePicker, Tag, Button as ButtonAn, Tooltip } from 'antd';
import { Input as InputStrap } from 'reactstrap';
import { Form as FormReact  , FormGroup, Label } from 'reactstrap';
import axios from "axios";
import LocationPicker from 'react-location-picker';

import profilePageStyle from "assets/jss/material-kit-react/views/profilePage.jsx";

const FormItem = Form.Item;
const CheckboxGroup = Checkbox.Group;
const { TextArea } = Input;

/* Default position */
const defaultPosition = {
    lat: -8.42950699707985,
    lng: 115.18595581054683
};

class ProfilePage extends React.Component {

    constructor(props) {
        super(props);
        let user  = reactLocalStorage.getObject('user');
        let daySelect = [];
        daySelect[0] = String.dayNow;


        this.state = {
            token: user.token,
            form: [],
            name: '',
            latitude: '',
            longitude: '',
            openTime: Config.openTime,
            closeTime: Config.closeTime,
            images:[],
            operationDay: [],
            daySelect: daySelect,
            description: '',
            phone: '',
            website: '',
            email: '',
            address: '',
            images: '',
            file: '',
            imagePreviewUrl: '',
            position: {
                lat: 0,
                lng: 0
            }
        };


        this._handleImageChange = this._handleImageChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
    }

    handleLocationChange ({ position, address }) {
        this.setState({ position, address });
    }

    componentWillMount() {

    }

    componentDidMount() {

    }

    setStateIni = (event, field) => {
        var stateObject = function() {
            let returnObj = {};
            returnObj[field] = event.target.value;
            return returnObj;
        }.bind(event)();

        console.log(stateObject);

        this.setState(stateObject);
    }

    setDaySelect = (event) => {
        let day = this.state.daySelect;
        //let key = 0;
        day = event.map((data) => {
            return data;
        });
        this.setState({
            daySelect: day
        });

    }

    onChange(e) {
        const images_browse = [];
        let count_browse = e.target.files.length;
        let i;
        for(i = 0; i < count_browse; i++) {
            images_browse[i] = e.target.files[i]
        }
        this.setState({
            //images: e.target.files[0]
            images: images_browse
        });
    }

    submit = () => {
        const self = this;
        var headers = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': this.state.token
                }
        }

        const formData = new FormData();
        const images = this.state.images;
        let no = 0;

        formData.append('name', this.state.name);
        formData.append('latitude', this.state.position.lat);
        formData.append('longitude', this.state.position.lng);
        formData.append('openTime', this.state.openTime);
        formData.append('closeTime', this.state.closeTime);
        formData.append('operationDay', JSON.stringify(this.state.operationDay));
        formData.append('description', this.state.description);
        formData.append('phone', this.state.phone);
        formData.append('website', this.state.website);
        formData.append('email', this.state.email);
        formData.append('address', this.state.address);

        images.forEach(function(element) {
            formData.append('images['+no+']', element);
            no++;
        });

        axios.post('/warung/add', formData, headers)
            .then(function(response) {

                message.config({
                    top: 100,
                    duration: 2,
                    maxCount: 3,
                });

                let resp = response.data;
                if(!resp.success) {
                    const form_self = resp.data;
                    self.setState({
                        form: form_self
                    });

                    console.log(form_self);

                    message.error(resp.message, 2);
                } else {
                    self.setState({
                        form: [],
                        user: resp.data
                    });
                    message.success(resp.message, 2);
                    window.location.reload();
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }




    _handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        console.log(e.target.files);

        console.log(1);

        reader.onloadend = () => {
            console.log(2);
            this.setState({
                file: file,
                imagePreviewUrl: reader.result
            });
        }

        console.log(3);

        reader.readAsDataURL(file)
    }


  render() {
    //const { classes, ...rest } = this.props;
    const { classes } = this.props;
    const { form } = this.state;

    let daySelect = this.state.daySelect;
      const nameField = form['name'] !== undefined ? 'error' : '';
      const nameLabel = nameField === "error" ? form['name'] : '';
      const openTimeField = form['openTime'] !== undefined ? 'error' : '';
      const openTimeLabel = openTimeField === "error" ? form['openTime'] : '';
      const closeTimeField = form['closeTime'] !== undefined ? 'error' : '';
      const closeTimeLabel = closeTimeField === "error" ? form['closeTime'] : '';
      const operationDayField = form['operationDay'] !== undefined ? 'error' : '';
      const operationDayLabel = operationDayField === "error" ? form['operationDay'] : '';
      const descriptionField = form['description'] !== undefined ? 'error' : '';
      const descriptionLabel = descriptionField === "error" ? form['description'] : '';
      const phoneField = form['phone'] !== undefined ? 'error' : '';
      const phoneLabel = phoneField === "error" ? form['phone'] : '';
      const websiteField = form['website'] !== undefined ? 'error' : '';
      const websiteLabel = websiteField === "error" ? form['website'] : '';
      const emailField = form['email'] !== undefined ? 'error' : '';
      const emailLabel = emailField === "error" ? form['email'] : '';
      const addressField = form['address'] !== undefined ? 'error' : '';
      const addressLabel = addressField === "error" ? form['address'] : '';
      const imagesField = form['images'] !== undefined ? 'error' : '';
      const imagesLabel = imagesField === "error" ? form['images'] : '';

      let {imagePreviewUrl} = this.state;
      let $imagePreview = null;
      if (imagePreviewUrl) {
          $imagePreview = (<img src={imagePreviewUrl} width={200} />);
      }

    return (
      <div>
        <HeaderMenu/>
        <Parallax small style={{ height: "200px" }} filter image={require("assets/img/header.jpg")} />
        <div className={classNames(classes.main, classes.mainRaised)}>
          <div>
            <div className={classes.container}>
                <br /><br />
                <h2 align="center">Tambah Warung</h2>
                <br /><br />
              <GridContainer justify="center" className="profile-wrapper">
                <GridItem xs={12} sm={12} md={7} className={classes.navWrapper}>
                    <Form>
                        <FormItem
                            label="Nama Warung"
                            validateStatus={nameField}
                            help={nameLabel}
                        >
                            <Input onKeyUp={(e) => this.setStateIni(e, 'name')} />
                        </FormItem>
                        <FormItem 
                            label="Hari Buka"
                            validateStatus={operationDayField}
                            help={operationDayLabel}>
                            <CheckboxGroup options={String.dayList} defaultValue={daySelect} onChange={this.setDaySelect} />
                        </FormItem>
                        <FormItem 
                            label="Jam Buka"
                            validateStatus={openTimeField}
                            help={openTimeLabel}>
                            <TimePicker defaultValue={moment(this.state.openTime, 'HH:mm:ss')} onChange={this.changeOpenTime} /> &nbsp;&nbsp;sampai&nbsp;&nbsp;&nbsp;
                            <TimePicker defaultValue={moment(this.state.closeTime, 'HH:mm:ss')} onChange={this.changeCloseTime} />
                        </FormItem>
                        <FormItem
                            label="Telepon"
                            validateStatus={phoneField}
                            help={phoneLabel}
                        >
                            <Input onKeyUp={(e) => this.setStateIni(e, 'phone')}  />
                        </FormItem>
                        <FormItem
                            label="Website"
                            validateStatus={websiteField}
                            help={websiteLabel}
                        >
                            <Input onKeyUp={(e) => this.setStateIni(e, 'website')} />
                        </FormItem>
                        <FormItem
                            label="Email"
                            validateStatus={emailField}
                            help={emailLabel}
                        >
                            <Input onKeyUp={(e) => this.setStateIni(e, 'email')} />
                        </FormItem>
                        <FormItem
                            label="Alamat"
                            validateStatus={addressField}
                            help={addressLabel}
                        >
                            <Input onKeyUp={(e) => this.setStateIni(e, 'address')} />
                        </FormItem>
                        <FormItem
                            label="Deskripsi"
                            validateStatus={descriptionField}
                            help={descriptionLabel}
                        >
                            <TextArea rows={4} onKeyUp={(e) => this.setStateIni(e, 'description')} />
                        </FormItem>
                        <FormItem
                            label="Lokasi Warung"
                        >
                            <LocationPicker
                                containerElement={ <div style={ {height: '100%'} } /> }
                                mapElement={ <div style={ {height: '300px'} } /> }
                                defaultPosition={defaultPosition}
                                onChange={this.handleLocationChange}
                            />
                        </FormItem>
                        <FormItem
                            label="Gambar Warung"
                            validateStatus={imagesField}
                            help={imagesLabel}
                        >
                            {$imagePreview}
                            <div class="file-loading">
                                <Input  onChange={(e) => this.onChange(e)} id="browse-img-multiple" name="file-en[]" type="file" multiple  />
                            </div>
                        </FormItem>
                        <br /><br />
                        <Button color="warning" size="lg" onClick={this.submit}>
                            Simpan Warung
                        </Button>
                    </Form>
                </GridItem>
              </GridContainer>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default withStyles(profilePageStyle)(ProfilePage);
