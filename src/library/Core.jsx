export {setStateIni};

function setStateIni (event, field) {
    var stateObject = function() {
        let returnObj = {};
        returnObj[field] = event.target.value;
        return returnObj;
    }.bind(event)();

    this.setState(stateObject);
}