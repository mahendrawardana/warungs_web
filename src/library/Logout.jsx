import React from "react";
import { Redirect } from 'react-router-dom';
import {reactLocalStorage} from 'reactjs-localstorage';

class Logout extends React.Component {
    constructor(props) {
        super(props);
        // we use this to make the card to appear after the page has been rendered
        this.state = {

        };
    }

    componentDidMount() {
        reactLocalStorage.set('statusLogin', 'no');
        reactLocalStorage.setObject('user', {});
    }

    render() {
        return <Redirect to="/"/>;
    }

}

export default Logout;