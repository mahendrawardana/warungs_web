var date = new Date();
var dayNow = date.getDay();

const String = {
    dayNow: dayNow,
    labelUserIsOwner: 'Anda adalah Owner',
    dayListSelect: [0,1,2,3,4,5,6],
    kategoriMenu: [
        { label: 'Ayam', value: 'ayam' },
        { label: 'Babi', value: 'babi' },
        { label: 'Kambing', value: 'kambing' },
        { label: 'Laut', value: 'laut' },
        { label: 'Jawa', value: 'jawa' },
        { label: 'Bali', value: 'bali' },
        { label: 'Asia', value: 'asia' },
        { label: 'Chinese', value: 'chinese' },
        { label: 'Lalapan', value: 'lalapan' },
        { label: 'Dan Lain-Lain', value: 'dll' },
    ],
    kategoriMenuSelect: [
        'ayam',
        'babi',
        'kambing',
        'laut',
        'jawa',
        'bali',
        'asia',
        'chinese',
        'lalapan',
        'dll'
    ],
    dayList: [
        { label: 'Senin', value: 1 },
        { label: 'Selasa', value: 2 },
        { label: 'Rabu', value: 3 },
        { label: 'Kamis', value: 4 },
        { label: 'Jumat', value: 5 },
        { label: 'Sabtu', value: 6 },
        { label: 'Minggu', value: 0 },
    ]
};

export default String;