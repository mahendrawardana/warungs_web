const path = require('path')
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get('*', (req, res)=>{
    res.sendFile(path.join(__dirname, 'build/index.html'));
});
app.listen(3000);
console.log('Server API HookiPay run on port 8084...');